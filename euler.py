
import numpy as np # 벡터화 혹은 행렬 연산 할때 numpy 쓰는 이유 임 
from matplotlib import pyplot as plt

""" 메쉬 혹은 채워넣을 그리드 형태
y'= -y + sin(x) , y(0) = 1, domain [1,10], nunber partion of domain 101
incerement 0.1  """

x_0 = 0
y_0 = 1
x_final = 10
n_step = 100
delta_x = ( x_final - x_0) / ( n_step - 1 )

x = np.linspace ( x_0 , x_final , n_step )
#vectorize x and initialize x
y = np.zeros([n_step])
# vectorize y and initialize y
y[0] = y_0

"""
오일러 방법으로 
y' = f(x,y)
y(x0) = y0

x_{n+1} = x_{n} + h
y_{n+1} = y_{n} + f( x_{n}, y_{n} ) = y_{n} + y'_{n} * h  
 """
for i in range(1,n_step):
	y[i] = delta_x * (-y[i-1] + np.sin(x[i])) + y[i-1]
for i in range(n_step):
	print(x[i],y[i])

plt.plot(x,y,'o')
plt.xlabel(" Value of x ")
plt.ylabel(" Value of y ")
plt.title(" Aprroximate Solution with Forward Euler 's Method " )
plt.show()

