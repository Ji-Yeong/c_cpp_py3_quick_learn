#!/usr/bin/python3	

import numpy as np # 수치 해석용패키지 왠만하면 수학 라이브러 보다는 이걸 쓰는것을 추천 
from matplotlib import pyplot as plt # 데이터 시각화 패키지 , 데이터베이스 용 패키지는 pandas , 기계학습 패키지는 검색 추천
# import 냐 from 의 차이는 바로 객체로 선언 할꺼냐 아니 내장 함수처럼 선언 할꺼냐 C++ 말하면 네임드 스페이스 선언 여부 따라 범위 지정 연산자 쓰는것을 생각해보라

n = 100000
#c = 29979245800 #[cm/s]
c = 1
c1 = 0.9999999999999*c 
v0 = 0.0001*c # [cm/s] speed of incident particle at start point , for plot 

#parameter for given material [cu]
a = 0.1535 # equald to (2pi*avogadro`#* classical electron radius ^2 * electrton mass * light speed ^2)  [MeV*cm^2/g]
rho = 9 # [g/cm^3]
z = 4 # charge of incident in unit of e
Z = 29 # effective atomic number of absorbing material
A = 65 # effecitve atomic weight of absorbing material
delta_corr = 4.42 # density correction
shell_corr = 2.111 # shell correction
I = 0.000326 # mean excitation potential [MeV]
m_e = (0.511/np.square(c)) # electron mass [Mev/c^2] # 복잡한 수식을 제곱하는경우 square() 함수 객체 쓰는것 추천

#variable
v = np.linspace(v0,c1,n) # 처음 과 끝 을 n 개 나누어서 n 개의 1차원 배열에 (끝-처음)/n 값만큼 오름차순으로 채움--> 벡터화
#v[0]= v0

beta = np.array(v/c) # beta 를 배열 선언
gamma = 1 / np.sqrt((1-np.square(beta)))  # 
eta =  gamma * beta 
W_max =  1.022*np.square(beta)*np.square(gamma)

stopping_add1 = np.zeros([n])
bethe_bloch1 = np.zeros([n])

stopping_basic = np.zeros([n]) # 벡터화 시킴 n 개배열에 0으로 초기화 
stopping_add = np.zeros([n])
bethe_bloch = np.zeros([n])


def bethe_fun(i):#함수 정의
	stopping_basic[i] = a*rho*Z/A*np.square(z)/np.square(beta[i])
	#stopping_add[i] = np.array((np.log(((2*m_e*np.square(gamma[i])*np.square(v[i])*W_max[i])/np.square(I))) - 2*np.square(beta[i]) - delta_corr - 2*(shell_corr/Z)))
	stopping_add[i] =  np.log( 2*m_e*np.square(v[i])*W_max[i] / (I*I*(1-np.square(beta[i])) ) ) - 2*np.square(beta[i]) - delta_corr - 2*(shell_corr/Z)
	bethe_bloch[i] = stopping_basic[i] * stopping_add[i]
	return bethe_bloch

def bethe_fun1(i):#함수 정의
	stopping_basic[i] = a*rho*Z/A*np.square(z)/np.square(beta[i])
	#stopping_add[i] = np.array((np.log(((2*m_e*np.square(gamma[i])*np.square(v[i])*W_max[i])/np.square(I))) - 2*np.square(beta[i]) - delta_corr - 2*(shell_corr/Z)))
	stopping_add1[i] =  np.log( 2*m_e*np.square(v[i])*W_max[i] / (I*I*(1-np.square(beta[i])) ) ) - 2*np.square(beta[i]) 
	bethe_bloch1[i] = stopping_basic[i] * stopping_add1[i]
	return bethe_bloch1


for i in range(1,n):#반복문 형태
	beta
	gamma
	eta
	stopping_basic
	stopping_add
	bethe_fun(i)
	stopping_add1
	bethe_fun1(i)

#visualize
plt.subplot(1,2,1)# 1 by 2 형태로 그림을 나누어서 그림 그중 1 그림
#plt.plot(v, v, label = r'incident speed',ls = ':',""" marker='>', """color = 'r' )
plt.plot(v, v, label = r'incident speed',ls = ':',color = 'r' )
plt.plot(v, beta, label = r' $\beta$ ',ls = '--', color = 'b' ,linewidth= 2.5 )
plt.plot(v, gamma, label = r' $\gamma$ ' , color = 'g')
plt.plot(v, eta, label = r' $\eta$=$\gamma$*$\beta$ ', linestyle = '-.',color = 'y', linewidth =2.0)
plt.ylim(0.01,10)
plt.xlim(0.001,3)
plt.xlabel('ratio of v/c')
plt.ylabel('number')
plt.xscale('log')
plt.legend(loc = 'best')

plt.subplot(1,2,2)
plt.plot(eta, bethe_fun(i),label = 'bethe-bloch w/ log + corr. term', color = 'b')
plt.plot(eta, bethe_fun1(i),label = 'bethe-bloch w/ log', color = 'c')
plt.plot(eta, stopping_basic, label  = 'basic factor ',linestyle = '--' , color = 'g')
plt.plot(eta, stopping_add, label = 'add term(log + corr.) ', linestyle = '-.',color = 'm')
plt.plot(eta, stopping_add1, label = 'add term(log) ', linestyle = '-.',color = 'y')
#plt.xlim(0.01,3)
plt.legend(loc='best')
plt.xlabel(r' $\eta$= $\beta$*$\gamma$ = p$/$Mc[MeV/c]')
plt.ylabel(r'stopping power [Mev cm^2/g] ')
plt.xscale('log')
plt.yscale('log')


plt.show()
