//c++11 문법으로 작성된 stopping power의 c99 버전 예제
#include <stdio.h>	// 매크로 혹은 천처기
#include <math.h>	// c 언어에서 정의된 일반적인 수학 함수
//빌드할때  명시적으로 수학 라이브러리를 링크하라는 옵션(-lm)을 넣어야함 gcc bethe_bloch.c -lm
#include <malloc.h>	// 동적할당이 구현된 라이브러리

//변수 초기화 및 설정
double c = 1.0;
double v_t = 0.999999999999999;
double v_i = 0.0001;
double a = 0.1535;
double rho = 9;
double z = 4;
double Z = 29;
double A = 65;
double delta_corr = 4.42;
double shell_corr = 2.111;
double I = 0.000326;
double m_e = 0.511;	//not divide c^2
int n = 1000000;	// 배열 혹은 벡터화 하기 위해서
double dv = 0.0;
int i = 0; //for loop counter
int main(void)		// visual studio 쓰는 사람은 arg은 없는데 리눅스 상에는 명시적으로 표현
{
	// [row][column] 이런식으로 정적할당 하는것 대신 아 N 차원 배열을 동적 할당을 하는 방법이 궁금할텐데은 방법은 포인터 변수 선언을 이용하는것임.
	v_i= v_i * c;
	m_e = 0.511/(c*c);	//보통은 pow 함수 써서 표현하는데 간단해서 이걸로 표현
	dv = (double)(v_t - v_i)/n ;
	double *v;
	v = malloc(sizeof(double) * n);	// double v[n]; 같은 표현임.  v벡터를 저장할려고 하는 자료구조를 동적할당 할려고함 그래서 포인터 변수로 선언 C 언어에서는 동적할당을 malloc,free 함수로 구현이되어 있음
	//v = {0,};	// v벡터의 모든 계수(또는 배열)을 0으로 초기화
	double *beta = (double *)malloc(sizeof(double) * n);
	//beta = {0,};
	double *gamma = (double *)malloc(sizeof(double) * n);	//위와 같음
	double *eta = (double *)malloc(sizeof(double) * n);
	double *W_max = (double *)malloc(sizeof(double) *n); 
	//gamma = {0}; //{0} 배열의 모든 성분을 0 초기화 {0,}와 똑같은 표현
	double *stopping_basic = (double *)malloc(sizeof(double) * n);
	double *stopping_add = (double *)malloc(sizeof(double) * n);
	double *stopping_add1 = (double *)malloc(sizeof(double) * n);
	double *bethe_bloch = (double *)malloc(sizeof(double) * n);
	double *bethe_bloch1 = (double *)malloc(sizeof(double) * n);

	//개발자가 구현한 파일 입출력 데이터 형식 FILE 이고 포인터 변수형 선언
	FILE *fp = fopen("bethe_c.dat","w");	//"w" 인자는 쓰기 . 표준 입출력 데이터 형식의 자세한 GNU 컴파일러 문서 또는  cppreference.com 참조
	// 논리제어문 시작!
	for (i =0; i< n ; i++)	//;와 지시자 같은것 들은  프로그래밍 언어론 보면 노암  촘스키 할아버지가 논한 언어 형식들 있음.
	{
		v[i] = dv * i ;
		beta[i] = v[i]/c;
		gamma[i] = 1/sqrt(1-pow(beta[i],2));
		eta[i] = beta[i]*gamma[i];
		W_max[i] = 1.022*pow(eta[i],2);
		stopping_basic[i] = a*rho*Z/A*pow(v[i],2)/pow(beta[i],2);
		stopping_add[i] = log(2*m_e*pow(v[i],2)*pow(gamma[i],2)*W_max[i]/pow(I,2)) - delta_corr - 2 * (shell_corr/Z);
		stopping_add1[i] = log(2*m_e*pow(v[i],2)*pow(gamma[i],2)*W_max[i]/pow(I,2));
		bethe_bloch[i] = stopping_basic[i] * stopping_add[i];
		bethe_bloch1[i] = stopping_basic[i] * stopping_add1[i];

		fprintf(fp," %lf	%lf	%lf\n", eta[i],bethe_bloch[i],bethe_bloch1[i]);	//파일 입출력 데이터 포인터 변수로  형식으로 선언된 fp에 해당된 인자를 파일상에 출력하여 기록

	}
	fclose(fp);


}
