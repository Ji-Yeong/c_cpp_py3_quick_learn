//프로그램 설명 베테 블로우 방정식의 해를 수치 계산 하는 간단한 코드이고 
//노인네 혹은 아재들^^이 애용한 이러한 프로그램에서 출력된 행렬 데이터 가지고 
//gnuplot 같은 그래프 그리는 프로그램으로 그래프 그림
/*
	어설픈 c++ 스타일 없음(c 와 c++ 문법 혼용 없음) 
	헤더 c++의 라이비러리(개발자가 구현한 함수를 의미) 헤더는 <> 이러한 꺽쇠로 표현해서 전처리 처리
	사용자 정의한 함수의 헤더는 " " 이러한 형식으로 표현해서 전처리 처리
	전처리 대한 내용은 검색하거나 첨부한 전산 개론 따라한 ppt 참조

	g++ -std=gnu++11 소스코드.cxx -o ELF 파일 이름 혹은 g++ -std=c++11 소스코드.cxx -o ELF 파일 이름
	여기서 std는 발표된 표준 문법 형식
	(실행예시) "g++ -std=gnu++11 bethe_bloch.cxx -o dEdx.out"
*/

#include <iostream>	// 파일 입출력 라이브러리 헤더
#include <vector>	// 벡터 형식으로 구현된 자료 구조 라이브러리 헤더
#include <cmath>	// 수학 계산 라이브러리 헤더
#include <fstream>	// 파일 입출력 라이브러리 헤더

//using namespace std; 
/*
일단 파일 입출력 네임스페이스 사용 안하고 코드 구현하고 헤더 파일과 네임스페이스 다른점은 
일반적으로 라이브러리 헤더파일속에 수많은 함수들이 정의됨 그리고 전처리하다보면 중복되는 함수와 외부(or정적)전역변수 충돌 가능성 있음
(사족: 이름 충돌 방지하기 위해 실험 하시는분들은 따로 네임 맹글링? 혹은 extern 변수 선언해서 임베딩(하드웨어) 환경에서 디텍터 R&D를 함 )
그걸 피하기 위해 c++에서 헤더 파일 속에 기능적으로 분류되어진 함수와 그에 따른 변수를 정의되어진 집합들 이루어진 제법 큰 클래스를 네임 스페이스라고 부름
네임스페이스를 자세한 내용은 문법책을.
*/

//매개변수(parameter) 선언 부분 만약 프로그램이 길어진다면 
//"변수의 활성 영역(scope of variables)"과 번수 선언 관습에 대해서 찾아 보시길
//modern c++  초기화 하는 방법이 3가지있는데  a = 10 , a(10) , a{10} 이걸 알면 c++ 중급 단계임

double c{1.0};	//c 빛의 속력을 1로 두고 
double v_t{0.99999999999999*c};	// 상대론적 효과를 보기위해 설정
double v_i{0.0001*c};
double a{0.1535};
double rho{9};
double z{4};
double Z{29};
double A{65};
double delta_corr{4.42};
double shell_corr{2.111};
double I{0.000326};
double m_e{0.511/(c*c)};

int n{1000000};
double dv{(v_t - v_i)/n};

//생성자 와 소멸자 선언 해야하는데 그거는 클래스 예제에서 사용하는 보여 줄 것임

int main()
{

/** C++ 언어의
	std::vector,array  안쓰고하면 
	C 언어 문법 형식으로 배열 및 각 배열요소 초기화 선언하면
	double *v = new double[n];	v = {0,};
	double *beta = new double[n];	beta = {0,};
	double *gamma = new double[n];	초기화 생략
	double *eta = new double[n];
	double *stopping_basic = new double[n];
	double *stopping_add = new double[n];
	double *bethe_bloch = new double[n];
	...생략
**/
	std::vector<double> v(n,0),beta(n,0),gamma(n,0),eta(n,0),W_max(n,0),stopping_basic(n,0),stopping_add(n,0),stopping_add1(n,0),bethe_bloch(n,0),bethe_bloch1(n,0);
	//선형적 배열을 n개 만큼 할당하고 그값을 0으로 선언 또한 <double>은 템플릿 변수라고 보면 됨 쉽게 말하면 int,double등 인자별로 함수들이 정의되어 있음
	//vector라는 자료구조 형식은 쉽게 말하면 C언어의 배열이라고 생각하면 되고 차이점은 배열은 고정이나 벡터는 가변적임 
	//전산학도들은 list 와 vector 큰 차이점인 벡터는 미리 자료형을 지정하여 중간 자료 삽입 및 제거 안되고 리스트는 무작위 접근이 안된다는 알고 있음 
	//그이유는 자료구조를 포인터로 구현하다 보면 알게 되는데 자료구조의 기본 형태인 리스트(이산,연속적)는 선형 혹은 비선형구조를 가지나 벡터는 선형 (연속) 구조임
	int i{0};
/**
	std::vector< std::vector<double> > mat(col , std::vector<double> (row,0) );	이런식으로 다차원 벡터 (행렬) 표현 가능함
	
	Double **mat = new double *[row];mat[]=new double [col];	결국 위의 2개 표현은mat[row][col] 의미함
**/
	std::ofstream dEdx1; //c++ 클래스 이용한 파일 입출력 클래스이고 그 클래스를 dEdx1 이라고 이름 지음 ,
						 // c 언어 스타일 파일 입출력 비교 해보시길 fopen();
	dEdx1.open("dEdx.dat");	// dEdx1 클래스의 멤버 함수 open 함수를 호출, open() 함수의 명세 보면 자세한 내용이 나와 있음
							//https://en.cppreference.com/w 혹은 http://www.cplusplus.com/ 
							

	for (i = 0 /*반복문 횟수에 관한 계수의 초기값*/; i < v.size() /* 반복문 종료 대한 계수값 조건(논리 연산) */; ++i/*논리 연산에 따른 계수값 연산*/)
	{
		v[i] = dv * i ;	//속도 혹은 beta가 베테 블로우 곡선을 그리면 x축에 해당하기 때문에 
		beta[i] = v[i]/c;
		gamma[i] = 1/std::sqrt(1-std::pow(beta[i],2));
		eta[i] = beta[i]*gamma[i];	// 교과서에 나오는 베터 블로우 곡선의 x축에 해당 (four velocity 생각하면)
		W_max[i] = 1.022*std::pow(eta[i],2);
		stopping_basic[i] = a*rho*Z/A*std::pow(z,2)/std::pow(beta[i],2);
		stopping_add[i] = std::log( 2*m_e*std::pow(v[i],2)*std::pow(gamma[i],2)*W_max[i]/std::pow(I,2))  - delta_corr - 2*(shell_corr/Z);
		stopping_add1[i] = std::log( 2*m_e*std::pow(v[i],2)*std::pow(gamma[i],2)*W_max[i]/std::pow(I,2) );
		bethe_bloch[i] = stopping_basic[i] * stopping_add[i];
		bethe_bloch1[i] = stopping_basic[i] * stopping_add1[i];
	//귀찮아서 반복문에 식을 그대로 섰는데 코드를 조금더 간결하게 하고 싶으면 베테 블로흐 결과식을 반환하는 사용자 정의 함수를 구현 해도됨.
	//코드 품질은 소스코드에 쏟는 시긴에 비례함.

		if( i % 100 == 0)
		{
			std::cout << i<<"-th "<<"	"<<"eta : " << eta[i] <<"	"<<"dE/dx : " << bethe_bloch[i] <<"	"<< "basic factor : "<< stopping_basic[i] << "	" <<"add factor : " << stopping_add[i] << std::endl;
			//화면 출력 시간 생각보다 많이 걸리므로 그이유는 컴퓨터 구조 (레지스터와 메모리 와 버스 구조) 조건문과 모듈러 연산을 통해서 화면출력 빈도를 줄여서 하는것도 하나의 방법임
		}
		//std::cout <<"eta : " << eta[i] <<"	"<<"dE/dx : " << bethe_bloch[i] <<"	"<< "basic factor : "<< stopping_basic[i] << "	" <<"add factor : " << stopping_add[i] << std::endl;	
		dEdx1 << eta[i] <<"	"<< bethe_bloch[i] <<"	"<< bethe_bloch1[i] << std::endl; // cout 다르게 "dEdx1 << 변수"는 dEdx1.open("dEdx.dat")안 dEdx.dat 생성하고
																					  // dEdx.dat에 변수와 정규표현식을 저장한다.
		//dEdx1 << eta[i] <<"	"<< bethe_bloch[i] <<"	"<< bethe_bloch1[i] <<"	"<< stopping_basic[i] << "	" << stopping_add[i] << std::endl;	

	}	

	dEdx1.close();	//반복문 종료에 따른 dEdx.dat 저장 완료 후 파일 입출력 클래스 반환
	
	//	프로그램이 길어지면 항상 메모리 관리 신경 쓰여하는데 여기서는 프로그램이 작아서 그냥 예제용으로 적었음.
	v.clear(); beta.clear(); gamma.clear(); eta.clear(); W_max.clear(); stopping_basic.clear(); stopping_add.clear(); stopping_add1.clear(); bethe_bloch.clear(); bethe_bloch1.clear();
	std::vector<double>(v).swap(v);
	std::vector<double>(beta).swap(beta);
	std::vector<double>(gamma).swap(gamma);
	std::vector<double>(eta).swap(eta);
	std::vector<double>(W_max).swap(W_max);
	std::vector<double>(stopping_basic).swap(stopping_basic);
	std::vector<double>(stopping_add).swap(stopping_add);
	std::vector<double>(stopping_add1).swap(stopping_add1);
	std::vector<double>(bethe_bloch).swap(bethe_bloch);
	std::vector<double>(bethe_bloch1).swap(bethe_bloch1);
	//delete &v,&beta,&gamma,&eta,&W_max,&stopping_basic,&stopping_add,&stopping_add1,&bethe_bloch,&bethe_bloch1; 
	//이런다고 해서 벡터 메모리 해제 안됨(약간은 C스타일 스터움?)
	
	/** 위벡터 해제 방법동등한 방법은
	v.clear(); //clear content
	v.resize(0); //resize it to 0
	v.shrink_to_fit(); //reallocate memory
	**/

	return 0;  //true 값은 무엇? 0? 1?

}
