	//최대한 C++ 기법을  많이 담을려고 했으나 필자는 전산학도 아니라 고에너지 물리 전공이라...
	//불행하게도 여러 문법이 섞인 루트에서는 이러한 문법이 적용이 안 될 수 있음.
	//"g++ -std=gnu++11 class.cpp", "g++ -std=c++11 class.cpp"

	#define _USE_MATH_DEFINES // 정의된 수학 상수 사용. IEEE인가? 거기서 정한 기준들이 있음
	#include <iostream>
	#include <vector>
	#include <fstream>
	#include <cmath>

	using namespace std;

	//나중에 다시 루트 문법에 맞게 작성할 예정임 지금은 오로지 C++ 문법만으로 파일 입출력 연습
	//C 언어에서 구조체
	struct	summary //summary 라는 구조체를 정의
	{
		int n_part{0};	
		int reactor{0};
		double fol{0};
		double ri_ratio[4]={};	// {}에 공란이여도 모든 원소들을 0으로 초기화 
	};// Summary_1; /*정의된 구조체 형식은 Summary_1 라는 변수로 선언 */

	struct particle_info	// norminal, pdg number,  nominal, nominal, px, py, pz, mass, nominal, vx, vy, vz
	{
		int nominal1{0};
		int pdg_number{0};
		int nominal2{0};
		int nominal3{0};
		double px{0};
		double py{0};
		double pz{0};
		double ma{0};
		double nominal4{0};
		double vx{0};
		double vy{0};
		double vz{0};
	}; //Particle_info_1; /* 정의된 구조체 형식을 Particle_info_1 라는 변수로 선언 */

	/*
	C언어 같은 절차 지향 와 C++/Java 같은 객체 지향 언어의 차이점은 클래스 존재 여부 (or 데이터 접근 관점)만 알아도 어느정도 감을 익힘
	물론 전산학도들은 개념 및 구현(성능)에 대해서는 철학의 인식론 논쟁 만큼 과격하게 논쟁한다고 들었음
	*/

	//C 언어 구조체에 함수가 결합된것이 클래스라고 보면 비전산학도 관점에서 휼륭한 초기 접근임.
	class cal	
	{
	public:
		double px1, py1, pz1, P1, px2, py2, pz2, P2, C12, D21, B12; // 원래 이러한 변수들은 접근지시자가  public에 있으면 안됨 (예제만들기 위해)
		void set_values1(const double&,const double&,const double&);	//함수 원형만 선언함 자세한 명세는 밑에 작성함 이렇게 하는 이유는 가독성이 높은 코드 작성 형식(코딩스타일)유지 하나\
												//시간 관계상 코딩 스타일 뒤죽 박죽임.
		void set_values2(const double&,const double&,const double&,const double&,const double&,const double&); 
		// 아래 방법처럼 입력 인자를 const 데이터타입 &변수(메모리주소) 선언하여 입력 받아야한는데 이렇게 안하면 전산학도들은 개거품 물지만 그냥 했음

		cal();		//생성자(constructor)선언 ,  c언어에서 함수에 관련된 매개변수 인자는 모두 선언 해줘야하는데 이걸로 한번에 할 수 있음
					//물론 클래스안의 멤버 함수들의 변수들을 초기화 할 필요성 있을때지만.
		~cal();		//소멸자(destructor) , 앞서 본 베테 블로후 계산 예제 볼떄 메모리 확보할 때 엄청 복잡하게 한 것이 기억날것임
					//클래스안의 멤버 함수, 객체들을 메모리 확보를 위해서 한번 소멸 시킬때 많이 사용 
					//이러한 생성자와 소멸자는  public  영역에서 선언.

		double dot(const double &px1, const double &py1, const double &pz1)
			{
				P1 = sqrt(pow(px1,2)+pow(py1,2)+pow(pz1,2));				
				return P1;
			};
		double dot2(const double &px1, const double &py1, const double &pz1, 
					const double &px2, const double &py2, const double &pz2) 
			{
				P1 = sqrt(pow(px1,2)+pow(py1,2)+pow(pz1,2));
				P2 = sqrt(pow(px2,2)+pow(py2,2)+pow(pz2,2));
				C12 = (180/M_PI)*(px1*px2 + py1*py2 + pz1*pz2)/(P1*P2);
				return C12;
			};
		double dot3(const double &C12)	// 게으른 사람들은  템플릿오버로딩 배워보자~.
			{ 
				D21 = acos(C12);
				return D21;
			};
		double dot4(const double &px1, const double &py1)
			{
				B12 = (90/M_PI)*atan2(py1,px1);
				return B12;
			};
		virtual void future_cal(const double &px1,const double &py1){} ; // 가상함수는 주로 나중에 코드가 변경 될 것이나 기능이 추가 될 경우를 대비해서 현재 개발자가
		//나중에 작성 할 사람에게 이 함수의 의도를 전달 및 확장성 주도록 선언, 어떻게 보면 함수 원형 선언과 비슷함.
        //즉 기본 클래스에서 정의되는것이 아니라 유도(or 상속된) 클래스에서 재정의 되는 메소드(클래스에 종속된 함수)임
	private:
		double pt1, pt2, mt1, inv_m1, inv_m2, mt2, met1, met2, e1, e2; //private 지시자에 변수를 선언하여 함부로 못건들게 예를 들면 cal intte; intte.pt1=0  \
		//이러한 형식으로 pt1 값을 못건들게함 접근 할 수 있는 방법은  위에서 클래스 내부로 접근 할 수 있게 
		//set_value 함수 만들고 그 함수에 private:double pt1, pt2, mt1, inv_m1, inv_m2, mt2, met1, met2, e1, e2; 이라고 선언한 변수를 그 함수에서 
		//접근 가능하게  pt1=a ,pt2=b 이런 형식으로 선언
	};


	/***이부분은_넘어감(물리학도가_연구에서_자유자재로_쓴다면_CHEP,mcnet에참여해서_HEP_software_연구개발가자~***/
	
    //cal 클래스의 public만 상속 받은 유도된 클래스 induced_cal 선언
	/*
	class induced_cal : public cal
	{
	private:
		double i_pt1, i_pt2, i_mt1, i_inv_m1, i_inv_m2, i_mt2, i_met1, i_met2, i_e1, i_e2;
	public:
		induced_cal : public cal();
		~induced_cal : public cal();
		Virtual double cal_pt(const& px1,const& py1, const& pz1)
		//{	
		//	cal.set_values2(double,double,double,double,double,double); 
		//	cal.set_values1(double,double,double);
		//	cal.future_cal();
		//	return cal.fuure_cal();
		//};
		
	};
	induced_cal : public cal:induced_cal : public cal(){};
	induced_cal : public cal:~induced_cal : public cal(){};
	*/
	
    /***이부분은_넘어감(물리학도가_연구에서_자유자재로_쓴다면_CHEP,mcnet에참여해서_HEP_software_연구개발가자~***/

	//constructiors 통해서 클래스의 객체의 초기화 , 이것이 씨와 차별화된 한가지 이유
	cal::cal(){}; // 클래스 생성할때 구체적인 필요 따라 매개변수를 전달했기 때문에 선언해야함
	cal::~cal(){}; // 클래스 내부에 생성자를 선언 하였기 떄문에 반드시 선언해야함 

	//	member initializaion in constructors 및 혹시 변수를 참조지시자를 privete 변경 대비시 구현
	void	cal::set_values1(const double &a, const double &b,const double &c)
	{
		px1 = a;
		py1 = b;
		pz1 = c;
	}
	//	member initializaion in constructors
	void	cal::set_values2(const double &a, const double &b ,const double &c, 
							 const double &d, const double &e, const double &f)
	{
		px1 = a;
		py1 = b;
		pz1 = c;
		px2 = d;
		py2 = e;
		pz2 = f;
	}
	
int main() // 실질적인 주 함수 실행 c/c++에서 핵심적인 역활 
{
	struct	summary sum1;	//summary 이라고 정의한 "구조제 형식의 변수 sum1"를 선언
	struct	particle_info first;
	struct	particle_info second;
	struct	particle_info third;
	
	class	cal	neutrino;	//cal 이라고 정의한 "클래스 형식의 변수 neutrino"라고 선언
	class	cal	positron;
	class	cal	neutron;

	ifstream filein("U235_target.dat");	//파일 입출력 클래스 중에 파일 읽기 시작!!!!!
	ofstream fileout("direction.txt");	//파일 입출력 클래스 중에서 파일 저장 시작!!!

// (이론)전산 계산 쪽인 내용인데 그냥 넘어감 알고리즘 배우다보면 나옴
// lambda function (terminlogy from lambda calcalus)
//	[captures](parameters) -> return type {statement or body}
//	caprures meanning is reference type (call by value, call by refercence , call by address )
//	then 
//	() : call lambda fuction
	
	while(!filein.eof())	//파일의 끝부분eof 까지 계속 반복문 실행
	//베테 블로우 계산 할때 보았곘지만 이렇게 "">>,<<" "이러한 연산자를 다시 재정의 해서 U235_target.dat의 행렬 데이터를 순서대로 선언한 배열 구조에 저장
	{
		filein >>	sum1.n_part >> sum1.reactor >> sum1.fol >> sum1.ri_ratio[0] >> sum1.ri_ratio[1] >> sum1.ri_ratio[2] >> sum1.ri_ratio[3] ;	//filein.getline(); 동일한 효과로 한줄 씩 읽음 물론 열(col)수를다서야함.
		filein >>	first.nominal1 >> first.pdg_number >> first.nominal2 >> first.nominal3 >> first.px >> first.py >> first.pz >> first.ma >> first.nominal4 >>	first.vx	>>	first.vy	>>	first.vz ;
		filein >>	second.nominal1 >> second.pdg_number >> second.nominal2 >> second.nominal3 >> second.px >> second.py >> second.pz >> second.ma >> second.nominal4 >> second.vx >> second.vy >> second.vz;
		filein >>	third.nominal1 >> third.pdg_number >> third.nominal2 >> third.nominal3 >> third.px >> third.py >> third.pz >> third.ma >> third.nominal4 >> third.vx >> third.vy >> third.vz;
	
		//check scanning values  
		cout <<	sum1.n_part <<'	'<< sum1.reactor <<'	' << sum1.fol<<'	' << sum1.ri_ratio[0]<<'	' << sum1.ri_ratio[1]<<'	' << sum1.ri_ratio[2]<<'	' << sum1.ri_ratio[3] <<endl;	//filein.getline(); 동일한 효과로 한줄 씩 읽음 물론 열(col)수를다서야함.
		cout <<	first.nominal1 <<'	'<< first.pdg_number <<'	'<< first.nominal2 <<'	'<< first.nominal3 <<'	'<< first.px <<'	'<< first.py <<'	'<< first.pz <<'	'<< first.ma <<'	'<< first.nominal4 <<'	'<< first.vx<<'	'<< first.vy <<'	'<< first.vz <<'	'<<endl;
		cout <<	second.nominal1 <<'	'<< second.pdg_number <<'	'<< second.nominal2 <<'	'<< second.nominal3 <<'	'<< second.px <<'	'<< second.py <<'	'<< second.pz <<'	'<< second.ma <<'	'<< second.nominal4 <<'	'<< second.vx<<'	'<< second.vy <<'	'<< second.vz <<'	'<<endl;
		cout <<	third.nominal1 <<'	'<< third.pdg_number <<'	'<< third.nominal2 <<'	'<< third.nominal3 <<'	'<< third.px <<'	'<< third.py <<'	'<< third.pz <<'	'<< third.ma <<'	'<< third.nominal4 <<'	'<< third.vx<<'	'<< third.vy <<'	'<< second.vz <<'	'<<endl;

		//벡터들의 평균 벡터를 구하기 위함
		neutrino.P1 =	neutrino.dot( /*double var1, double var2, double var3 */first.px, first.py, first.pz);
		cout << neutrino.P1 <<endl;

		fileout << neutrino.P1; // 계산된 값을 저장

		/**
		음 아무거나 한번 작성을 해보시면 될듯.
		**/


	}

	filein.close();

		
};

//실행해서 확인 해보면 어떤 논리 연산의 오류가 있는가? 
		

