set terminal x11
set logscale xy
set ylabel 'stopping power [Mev cm^{2} /g] '
set xlabel '{/Symbol b}{/Symbol g}=p/Mc [MeV/c]'
plot "bethe_c.dat" using 1:2 w l lw 2 lc rgb 'blue'
#plot "dEdx.dat" using 1:2 w l lw 2 lc rgb 'blue', "dEdx.dat" using 1:3 w l lw 1 lc rgb 'red'
#plot [0.005:2] [80:600] "dEdx.dat" using 1:2 w l lw 2 lc rgb 'blue', "dEdx.dat" using 1:3 w l lw 1 lc rgb 'red'
#set terminal postscript eps color enhanced 'Helvetica,24'
#set out 'dEdx.eps'
replot
